﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RotTrans : MonoBehaviour
{
    public Quaternion quaternion;
    public GameObject cube;
    public float globalPositionX;
    public float globalPositionY;
    public float globalPositionZ;
    public float globalRotationX;
    public float globalRotationY;
    public float globalRotationZ;


    public Matrix4x4 m;
    public Vector3 translation;
    public Vector3 eulerAngles;
    public Vector3 scale = new Vector3(1, 1, 1);
    private MeshFilter mf;
    private Vector3[] origVerts;
    private Vector3[] newVerts;


    void Awake()
    {
        mf = GetComponent<MeshFilter>();
        origVerts = mf.mesh.vertices;
        newVerts = new Vector3[origVerts.Length];
        quaternion = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
        globalPositionX = cube.transform.position.x;
        globalPositionY = cube.transform.position.y;
        globalPositionZ = cube.transform.position.z;
        globalRotationX = cube.transform.rotation.x;
        globalRotationY = cube.transform.rotation.y;
        globalRotationZ = cube.transform.rotation.z;
    }

    void Update()
    {
        //Q and A move in the x position
       if (Input.GetKey(KeyCode.Q))
        {
            Vector3 position = cube.transform.position;
            position.x--;
            cube.transform.position = position;
        }
        if (Input.GetKey(KeyCode.A))
        {
            Vector3 position = cube.transform.position;
            position.x++;
            cube.transform.position = position;
        }
        //W  and S move in the y position
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 position = cube.transform.position;
            position.y--;
            cube.transform.position = position;
        }
        if (Input.GetKey(KeyCode.S))
        {
            Vector3 position = cube.transform.position;
            position.y++;
            cube.transform.position = position;
        }
        //E and D move in the z position
        if (Input.GetKey(KeyCode.E))
        {
            Vector3 position = cube.transform.position;
            position.z--;
            cube.transform.position = position;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Vector3 position = cube.transform.position;
            position.z++;
            cube.transform.position = position;
        }

        //Z,X,C,V are how you rotate the cube
        if (Input.GetKey(KeyCode.Z))
        {
            transform.Rotate(Vector3.up, Time.deltaTime*10);
            print(transform.eulerAngles.x);
        }
        if (Input.GetKey(KeyCode.X))
        {
            transform.Rotate(Vector3.up, Time.deltaTime*-10);
        }
        if (Input.GetKey(KeyCode.C))
        {
            transform.Rotate(Vector3.right, Time.deltaTime*10);
        }
        if (Input.GetKey(KeyCode.V))
        {
            transform.Rotate(Vector3.right, Time.deltaTime*-10);
        }

        //reset cube to 0,0,0,
        if (Input.GetKey(KeyCode.M))
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

       
        globalPositionX = cube.transform.position.x;
        globalPositionY = cube.transform.position.y;
        globalPositionZ = cube.transform.position.z;
        globalRotationX = cube.transform.rotation.x;
        globalRotationY = cube.transform.rotation.y;
        globalRotationZ = cube.transform.rotation.z;
        
        quaternion = cube.transform.rotation;

        Matrix4x4 m = Matrix4x4.TRS(translation, quaternion, scale);
        int i = 0;
        while (i < origVerts.Length) {
            newVerts[i] = m.MultiplyPoint3x4(origVerts[i]);
            i++;
        }
        mf.mesh.vertices = newVerts;
    }   
    
    void OnGUI()
    {

        // use eulerAngles to show the euler angles of the quaternion
        Vector3 quant = quaternion.eulerAngles;
        
        GUI.Label(new Rect(10, 10, 1000, 200), 
        "Globalpos" + "X: "+ globalPositionX +"    Globalpos" + "Y: " + globalPositionY + "    Globalpos" + "Z:" + globalPositionZ +
        "    Globalrot" + "X: "+ globalRotationX +"    Globalrot" + "Y: " + globalRotationY + "    Globalrot " + "Z:" + globalRotationZ + "    Quaternion:" + quant + "                                                            "+  m.ToString("F3"));

    
        // note that localEulerAngles will give the same result
        // GUI.Label(new Rect(10,90,250,50), cube.transform.localEulerAngles.ToString("F3"));
    }
}
