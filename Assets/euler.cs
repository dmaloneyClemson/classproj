﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class euler : MonoBehaviour
{
    // Assign an absolute rotation using eulerAngles
    float yRotation = 5.0f;

    void Start()
    {
        // Print the rotation around the global X Axis
        print(transform.eulerAngles.x);
        // Print the rotation around the global Y Axis
        print(transform.eulerAngles.y);
        // Print the rotation around the global Z Axis
        print(transform.eulerAngles.z);
    }

    void Update()
    {
        yRotation += Input.GetAxis("Horizontal");
        transform.eulerAngles = new Vector3(10, yRotation, 0);
    }
}
    